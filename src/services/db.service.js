import SQLite from "react-native-sqlite-storage";
import { DATABASE } from "../consts/database";

SQLite.DEBUG(true);
SQLite.enablePromise(true);

class Database {
  static initDB() {
    let db;
    return new Promise((resolve) => {
      console.log("Plugin integrity check ...");
      SQLite.echoTest().then(() => {
        console.log("Integrity check passed ...");
        console.log("Opening database ...");
        SQLite.openDatabase({
          name: "test1.db",
          location: "Documents",
        }).then(DB => {
          db = DB;
          console.log("Database OPEN");
          db.executeSql("SELECT 1 FROM House LIMIT 1").then(() => {
            console.log("Database is ready ... executing query ...");
          }).catch((error) => {
            console.log("Received error: ", error);
            console.log("Database not yet ready ... populating data");
            db.transaction((tx) => {
              tx.executeSql("CREATE TABLE IF NOT EXISTS House (houseId, houseTitle, houseType, houseAddress, houseBedrooms, houseFurnitureTypes, housePrice, houseNote, houseReporterName, email, phoneNumber)");
            }).then(() => {
              console.log("Table created successfully");
              resolve();
            }).catch(error => {
              console.log(error);
            });
          });
          resolve(db);
        }).catch(error => {
          console.log(error);
        });
      }).catch(error => {
        console.log("echoTest failed - plugin not functional");
      });
    });
  }

  static closeDatabase(db) {
    if (db) {
      console.log("Closing DB");
      db.close()
        .then(status => {
          console.log("Database CLOSED");
        })
        .catch(error => {
          // this.errorCB(error);
          console.log("err", error)
        });
    } else {
      console.log("Database was not OPENED");
    }
  }

  static addHouse(data) {
    return new Promise((resolve) => {
      this.initDB().then((db) => {
        db.transaction((tx) => {
          tx.executeSql("INSERT INTO House VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)", [data.houseId, data.houseTitle, data.houseType, data.houseAddress, data.houseBedrooms, data.houseFurnitureTypes, data.housePrice, data.houseNote, data.reporterName, data.email, data.phoneNumber]).then(([tx, results]) => {
            resolve(results);
          });
        }).then((result) => {
          this.closeDatabase(db);
        }).catch((err) => {
          console.log(err);
        });
      }).catch((err) => {
        console.log(err);
      });
    });
  }

  static getListHouse() {
    return new Promise((resolve) => {
      let houses = [];
      this.initDB().then((db) => {
        db.transaction((tx) => {
          tx.executeSql("SELECT * FROM House", []).then(([tx, results]) => {
            console.log("Query completed");
            let len = results.rows.length;
            for (let i = 0; i < len; i++) {
              let row = results.rows.item(i);
              houses.push({ ...row });
            }
            console.log("hello", houses);
            resolve(houses);
          });
        }).then((result) => {
          this.closeDatabase(db);
        }).catch((err) => {
          console.log(err);
        });
      }).catch((err) => {
        console.log(err);
      });
    });
  }

  static getHouseById(houseId) {
    console.log(houseId);
    return new Promise((resolve) => {
      this.initDB().then((db) => {
        db.transaction((tx) => {
          tx.executeSql("SELECT * FROM House WHERE houseId = ?", [houseId]).then(([tx, results]) => {
            console.log("houseInfo", results);
            if (results.rows.length > 0) {
              let row = results.rows.item(0);
              resolve(row);
            }
          });
        }).then((result) => {
          this.closeDatabase(db);
        }).catch((err) => {
          console.log(err);
        });
      }).catch((err) => {
        console.log(err);
      });
    });
  }


  static updateHouse(houseId, data) {
    return new Promise((resolve) => {
      this.initDB().then((db) => {
        db.transaction((tx) => {
          tx.executeSql(`UPDATE House SET 
            houseTitle = ?, houseType = ?, houseAddress = ?, houseBedrooms = ?, houseFurnitureTypes = ?, 
            housePrice = ?, houseNote = ?, houseReporterName = ?, email = ?, phoneNumber = ? WHERE houseId=?`,
            [data.houseTitle, data.houseType, data.houseAddress, data.houseBedrooms,
              data.houseFurnitureTypes, data.housePrice, data.houseNote,
              data.reporterName, data.email, data.phoneNumber, houseId]).then(([tx, result]) => {
            resolve(result);
          });
        }).then(result => {
          this.closeDatabase(db);
        }).catch(err => {
          console.log(err);
        });
      }).catch(err => {
        console.log(err);
      });
    });
  }

  static deleteHouse(houseId) {
    return new Promise(resolve => {
      this.initDB().then(db => {
        db.transaction(tx => {
          tx.executeSql("DELETE FROM House WHERE houseId = ?", [houseId]).then(([tx, results]) => {
            console.log(results);
            resolve(results);
          });
        }).catch(err => {
          console.log("err", err);
        });
      }).catch(err => {
        console.log(err);
      });
    });
  }

  static searchHouse(field, value) {
    let houses = [];
    return new Promise(resolve => {
      this.initDB().then(db => {
        db.transaction(tx => {
          tx.executeSql(`SELECT * FROM House WHERE ${field} LIKE '%${value}%'`).then(([tx, results]) => {
            for (let i = 0; i < results.rows.length; i++) {
              let row = results.rows.item(i);
              houses.push({ ...row });
            }
            resolve(houses);
          }).catch(err => {
            console.log("err", err);
          });
        });
      }).catch(err => {
        console.log("err", err);
      });
    });
  }


  static createNoteTable() {
    let db;
    return new Promise((resolve) => {
      SQLite.openDatabase({
        name: "test1.db",
      }).then(DB => {
        db = DB;
        db.executeSql("SELECT 1 FROM Note LIMIT 1").then(() => {
          console.log("Database is ready ... executing query ...");
        }).catch((error) => {
          console.log("Received error: ", error);
          console.log("Database not yet ready ... populating data");
          db.transaction((tx) => {
            tx.executeSql("CREATE TABLE IF NOT EXISTS Note (noteId, houseId, houseType, houseTitle, content, time)");
          }).then(() => {
            console.log("Table created successfully");
            resolve();
          }).catch(error => {
            console.log(error);
          });
        });
        resolve(db);
      }).catch(error => {
        console.log(error);
      });
    });
  }

  static addNote(data) {
    return new Promise((resolve) => {
      console.log("add note")
      this.createNoteTable().then((db) => {
        db.transaction((tx) => {
          tx.executeSql("INSERT INTO Note VALUES (?, ?, ?, ?, ?, ?)", [data.noteId, data.houseId, data.houseType, data.houseTitle, data.content, data.time]).then(([tx, results]) => {
            resolve(results);
          });
        }).then((result) => {
          this.closeDatabase(db);
        }).catch((err) => {
          console.log(err);
        });
      }).catch((err) => {
        console.log(err);
      });
    });
  }

  static getNoteList() {
    return new Promise((resolve) => {
      let notes = [];
      this.createNoteTable().then((db) => {
        db.transaction((tx) => {
          tx.executeSql("SELECT * FROM Note", []).then(([tx, results]) => {
            console.log("Query completed");
            let len = results.rows.length;
            for (let i = 0; i < len; i++) {
              let row = results.rows.item(i);
              notes.push({ ...row });
            }
            console.log("hello", notes);
            resolve(notes);
          });
        }).then((result) => {
          this.closeDatabase(db);
        }).catch((err) => {
          throw error;
          console.log(err);
        });
      }).catch((err) => {
        throw error;
        console.log(err);
      });
    });
  }

  static searchNote(value) {
    let notes = [];
    return new Promise(resolve => {
      this.createNoteTable().then(db => {
        db.transaction(tx => {
          tx.executeSql(`SELECT * FROM Note WHERE houseType = ? `, [value]).then(([tx, results]) => {
            for (let i = 0; i < results.rows.length; i++) {
              let row = results.rows.item(i);
              notes.push({ ...row });
            }
            resolve(notes);
          }).catch(err => {
            console.log("err", err);
          });
        });
      }).catch(err => {
        console.log("err", err);
      });
    });
  }

  static deleteNote(noteId) {
    return new Promise(resolve => {
      this.createNoteTable().then(db => {
        db.transaction(tx => {
          tx.executeSql("DELETE FROM Note WHERE noteId = ?", [noteId]).then(([tx, results]) => {
            console.log(results);
            resolve(results);
          });
        }).catch(err => {
          console.log("err", err);
        });
      }).catch(err => {
        console.log(err);
      });
    });
  }

}

export default Database;
