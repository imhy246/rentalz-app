const category = [
  {
    id: "all",
    title: "All",
    icon: "border-all"
  },
  {
    id: "house",
    title: "House",
    icon: "home"
  },
  {
    id: "flat",
    title: "Flat",
    icon: "city"
  },
  {
    id: "room",
    title: "Room",
    icon: "store"
  },
]
export default category
