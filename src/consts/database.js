export const DATABASE = {
  database_name: "HouseDatabase.db",
  database_version: "1.0",
  database_displayName: "SQLite React Offline Database",
  database_size: 200000,
};
