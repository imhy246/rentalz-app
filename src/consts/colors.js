const COLORS = {
  white: '#FFF',
  whiteLight: '#f1f0f0',
  dark: '#000',
  light: '#f6f6f6',
  grey: '#A9A9A9',
  grey1: '#979797',
  blue: '#5f82e6',
  green: '#5daf52',
  blueLight: '#d4e1ff',
  red: 'red',
  primary: "#422343",
  tranparent: 'rgba(0,0,0,0)',

};
export default COLORS;
