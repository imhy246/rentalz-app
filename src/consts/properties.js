const properties = [
  {
    id: "houseTitle",
    label: "House Title",
    placeHolder: "Enter title...",
  },
  {
    id: "houseType",
    label: "House Type",
    placeHolder: "Enter type...",
  },
  {
    id: "houseAddress",
    label: "Address",
    placeHolder: "Enter address...",
  },
  {
    id: "houseBedrooms",
    label: "Bedrooms",
    placeHolder: "Enter number...",
  },
  {
    id: "housePrice",
    label: "Price",
    placeHolder: "Enter price...",
  },
  {
    id: "houseReporterName",
    label: "Reporter",
    placeHolder: "Enter reporter name...",
  },
];
export default properties;
