const houses = [
  {
    id: '1',
    type: "home",
    price: "$1500",
    title: 'House for rent',
    location: 'So 2, ngach 1, Cau giay',
    image: require('../assets/images/house1.jpg'),
    details: `This building is located in the Oliver area, withing walking distance of shops...`,
    interiors: [
      require('../assets/images/interior1.jpg'),
      require('../assets/images/interior2.jpg'),
      require('../assets/images/interior3.jpg'),
    ],
  },
  {
    id: '2',
    type: "home",
    price: "$1500",
    title: 'Private room for rent',
    location: 'My Dinh, Ha Noi',
    image: require('../assets/images/house2.jpg'),
    details: `This building is located in the Oliver area, withing walking distance of shops...`,
    interiors: [
      require('../assets/images/interior1.jpg'),
      require('../assets/images/interior2.jpg'),
      require('../assets/images/interior3.jpg'),
    ],
  },
  {
    id: '3',
    type: "home",
    price: "$1500",
    title: 'Flat for rent',
    location: 'Trung Hoa, Cau Giay',
    image: require('../assets/images/house3.jpg'),
    details: `This building is located in the Oliver area, withing walking distance of shops...`,
    interiors: [
      require('../assets/images/interior1.jpg'),
      require('../assets/images/interior2.jpg'),
      require('../assets/images/interior3.jpg'),
    ],
  },
  {
    id: '4',
    type: "home",
    price: "$1500",
    title: 'Home for Rent 3',
    location: 'Nhan My, My Dinh',
    image: require('../assets/images/house4.jpg'),
    details: `This building is located in the Oliver area, withing walking distance of shops...`,
    interiors: [
      require('../assets/images/interior1.jpg'),
      require('../assets/images/interior2.jpg'),
      require('../assets/images/interior3.jpg'),
    ],
  },
];

export default houses;
