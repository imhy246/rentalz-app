class Validation {
  static validationEmail(email) {
    console.log("email", email)
    {
      // ^: start of line
      // $: email of line
      // \w: Any word character (letter, number, underscore)
      // [a-z]	Any single character in the range a-z
      // ? : The previous character can be repeated 0 or 1 times
      // + : The previous character can be repeated 1 or many times
      // * : The previous character can be repeated 0 or many times

      let regexEmail = /^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/;
      if (email.match(regexEmail)) {
        console.log("hello")
        return true;
      } else {
        return false;
      }
    }
  }

  static isValidPhone(phoneNumber) {
    let found = phoneNumber.search(/^(\+{1}\d{2,3}\s?[(]{1}\d{1,3}[)]{1}\s?\d+|\+\d{2,3}\s{1}\d+|\d+){1}[\s|-]?\d+([\s|-]?\d+){1,2}$/);
    if(found > -1) {
      return true;
    }
    else {
      return false;
    }
  }

  static isValidPrice(price) {
    let found = price.search(/^\d+(?:\.\d+)?(?:,\d+(?:\.\d+)?)*$/);
    if(found > -1) {
      return true;
    } else {
      return false
    }
  }
}
export default Validation;
