import React, { useState } from "react";
import { Button, Dimensions, ImageBackground, StyleSheet, Text, TextInput, View } from "react-native";
import COLORS from "../../consts/colors";
import { Controller, useForm } from "react-hook-form";
import Error from "../components/error";
import Icon from "react-native-vector-icons/FontAwesome5";

const height = Dimensions.get("window").height;

const Login = props => {
  const [errorMessage, setErrorMessage] = useState(null);
  const [showPassword, setShowPassword] = useState(false);
  const { control, register, setValue, getValues, handleSubmit, reset, formState: { errors } } = useForm({
    reValidateMode: "onChange",
    mode: "onChange",
    defaultValues: {
      username: "",
      password: "",
    },
  });

  const onSubmit = (values) => {
    if (values.username !== "rentalZ" || values.password !== "admin@123") {
      setErrorMessage("Wrong username or password");
    } else {
      props.navigation.navigate("Overview");
    }
  };

  return (
    <View style={{ flex: 1 }}>
      <View style={{ flex: 1, backgroundColor: COLORS.white }}>
        <ImageBackground style={styles.image} source={require("../../assets/images/house3.jpg")} />
      </View>
      <View style={styles.main}>
        <View style={{ alignItems: "center", marginTop: 15 }}>
          <Text style={{ fontSize: 24, fontWeight: "bold", color: COLORS.dark }}>Login</Text>
          <View style={{ flexDirection: "row", marginTop: 8 }}>
            <Text style={{ marginRight: 8 }}>Don't have an account yet?</Text>
            <Text style={{ color: COLORS.blue }}>Sign Up</Text>
          </View>
        </View>
        <View>
          <View style={{ marginTop: 50 }}>
            {/*Title Input*/}
            <View style={{ marginBottom: 15 }}>
              {errorMessage && (
                <Text style={{
                  padding: 8,
                  backgroundColor: COLORS.red,
                  color: COLORS.white,
                  borderRadius: 8,
                  fontWeight: "400",
                }}>{errorMessage}</Text>
              )}
            </View>
            <Controller
              control={control}
              rules={{
                required: true,
              }}
              render={({ field: { onChange, onBlur, value } }) => (
                <View>
                  <Text style={styles.label}>Username</Text>
                  <TextInput
                    onFocus={() => setErrorMessage(null)}
                    placeholder="Input username..."
                    style={styles.input}
                    onBlur={onBlur}
                    onChangeText={onChange}
                    value={value}
                    {...register("username", {
                      required: "Username is required.",
                      minLength: {
                        value: 4,
                        message: "Username must be at least 4 characters.",
                      },
                    })}
                  />
                </View>
              )}
              name="username"
            />
            {errors.username && <Error>{errors.username.message}</Error>}
            <Controller
              control={control}
              rules={{
                required: true,
              }}
              render={({ field: { onChange, onBlur, value } }) => (
                <View>
                  <Text style={styles.label}>Password</Text>
                  <TextInput
                    onFocus={() => setErrorMessage(null)}
                    placeholder="Input password..."
                    style={styles.input}
                    onBlur={onBlur}
                    secureTextEntry={!showPassword}
                    onChangeText={onChange}
                    value={value}
                    {...register("password", {
                      required: "Password is password.",
                    })}
                  />
                  <Icon name={showPassword ? "eye" : "eye-slash"} onPress={() => setShowPassword(!showPassword)}
                        style={{
                          position: "absolute",
                          right: 8,
                          bottom: 20,
                        }} />
                </View>
              )}
              name="password"
            />
            {errors.password && <Error>{errors.password.message}</Error>}
            <View style={{ marginTop: 12 }}>
              <Button title="Submit" color={COLORS.blue} onPress={handleSubmit(onSubmit)} />
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  image: {
    height: height * 0.6,
    width: "100%",
  },
  input: {
    paddingHorizontal: 15,
    height: 45,
    marginBottom: 5,
    backgroundColor: COLORS.light,
    borderRadius: 8,
  },
  label: {
    color: COLORS.dark,
    fontWeight: "bold",
    marginBottom: 5,
    opacity: 0.7,
  },
  main: {
    flex: 1,
    backgroundColor: COLORS.white,
    marginTop: -(height * 0.2),
    borderTopRightRadius: 30,
    borderTopLeftRadius: 30,
    paddingHorizontal: 40,
  },
});
export default Login;
