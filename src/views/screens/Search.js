import React, { useEffect, useState } from "react";
import { Text, TextInput, View, StyleSheet, FlatList, TouchableOpacity, Image, Pressable } from "react-native";
import COLORS from "../../consts/colors";
import Icon from "react-native-vector-icons/FontAwesome5";
import properties from "../../consts/properties";
import Database from "../../services/db.service";
import Spinner from "react-native-loading-spinner-overlay";

const Search = props => {
  const [selectedProperty, setSelectedProperty] = useState("houseTitle");
  const [query, setQuery] = useState("");
  const [houses, setHouses] = useState([]);
  const [visible, setVisible] = useState(false);
  const search = (property, value) => {
    setVisible(true);
    console.log(property, value);
    Database.searchHouse(property, value).then(res => {
      if (res) {
        setHouses(res);
        setVisible(false);
      }
    }).catch(err => {
      console.log("err", err);
      setVisible(false);
    });
  };

  const renderItem = ({ item }) => {
    const selected = selectedProperty === item.id;
    return (
      <TouchableOpacity
        style={{
          backgroundColor: selected ? COLORS.dark : COLORS.light,
          padding: 4,
          borderRadius: 16,
          alignItems: "center",
          justifyContent: "center",
          marginRight: 12,
        }}
        onPress={() => {
          setSelectedProperty(item.id);
        }}
      >
        <View style={{
          width: 90,
          height: 30,
          borderRadius: 20,
          alignItems: "center",
          justifyContent: "center",
        }}>
          <Text style={{
            fontWeight: "bold",
            color: selected ? COLORS.white : COLORS.primary,
          }}>{item.label}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  const renderHomeCard = ({ item }) => {
    console.log("house", item);
    return (
      <TouchableOpacity
        style={{
          marginBottom: 16,
          borderRadius: 16,
          backgroundColor: COLORS.white,
          marginHorizontal: 20,
          elevation: 10,
        }}
        onPress={() => {
          props.navigation.navigate("DetailScreen", item);
        }}
      >
        <View style={style.card}>
          <Image source={require("../../assets/images/house3.jpg")} style={style.cardImage} resizeMode="cover" />
          <View style={{ margin: 10, flex: 1 }}>
            <Text
              style={{ fontSize: 17, fontWeight: "bold", color: COLORS.dark, marginBottom: 4 }}>{item.houseTitle}</Text>
            <View style={{ flexDirection: "row", marginBottom: 4 }}>
              <Text style={{ fontWeight: "bold", marginRight: 8, color: COLORS.primary }}>${item.housePrice}</Text>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Icon name="bed" size={14} color={COLORS.primary} style={{ marginRight: 3 }} />
                <Text style={{ color: COLORS.primary }}>{item.houseBedrooms}</Text>
              </View>
            </View>
            <View style={{ flexShrink: 1 }}>
              <Text numberOfLines={1}>{item.houseAddress}</Text>
            </View>
          </View>
          <View style={{ flexDirection: "column", alignItems: "center", justifyContent: "center", marginRight: 8 }}>
            <Pressable style={{
              height: 30,
              width: 30,
              backgroundColor: COLORS.blue,
              borderRadius: 8,
              justifyContent: "center",
              alignItems: "center",
              paddingVertical: 4,
              paddingHorizontal: 4,
              marginBottom: 4,
            }}>
              <Icon name="pen" size={12} color={COLORS.white} />
            </Pressable>
            <Pressable style={{
              height: 30,
              width: 30,
              backgroundColor: COLORS.red,
              borderRadius: 8,
              justifyContent: "center",
              alignItems: "center",
              paddingVertical: 4,
              paddingHorizontal: 4,
            }}>
              <Icon name="trash" size={12} color={COLORS.white} />
            </Pressable>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={{ flex: 1, backgroundColor: COLORS.white }}>
      <Spinner
        visible={visible}
        textContent="Processing..."
        textStyle={{
          fontSize: 14,
          color: COLORS.white,
        }} />
      <View
        style={{
          flexDirection: "row",
          justifyContent: "space-between",
          paddingHorizontal: 20,
          paddingVertical: 20,
        }}>
        <View style={style.searchInputContainer}>
          <TextInput placeholder="Search address, title, reporter..." onChangeText={text => setQuery(text)}
                     defaultValue={query} />
        </View>
        <TouchableOpacity style={style.sortBtn} onPress={() => {
          search(selectedProperty, query);
        }}>
          <Icon name="search" color={COLORS.white} size={25} />
        </TouchableOpacity>
      </View>

      <View style={{ paddingHorizontal: 20 }}>
        <FlatList
          data={properties}
          renderItem={renderItem}
          horizontal
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item) => item.id}
          contentContainerStyle={{ paddingVertical: 8, paddingHorizontal: 0 }}
        />
      </View>
      {houses.length === 0 ? (
        <View style={{flex: 1, alignItems: "center", justifyContent: "center"}}>
          <Text style={{color: COLORS.grey}}>No results</Text>
        </View>
      ) : (
        <View style={{ flex: 1 }}>
         <View style={{display: "flex", flexDirection: "row" , justifyContent: "space-between"}}>
           <Text style={{ fontWeight: "bold", fontSize: 18, color: COLORS.dark, paddingHorizontal: 20 }}>Result
             List</Text>
           <Text style={{fontSize: 14, color: COLORS.dark, paddingHorizontal: 20 }}>{houses.length} result(s)</Text>
         </View>
          <FlatList
            data={houses}
            renderItem={renderHomeCard}
            keyExtractor={(item) => item.houseId}
            contentContainerStyle={{ paddingVertical: 8, paddingHorizontal: 4, marginBottom: 20 }}
            showsVerticalScrollIndicator={true}
          />
        </View>
      )}
    </View>
  );
};

const style = StyleSheet.create({
  card: {
    height: 100,
    flexDirection: "row",
  },
  cardImage: {
    height: 100,
    width: "30%",
    borderTopLeftRadius: 16,
    borderBottomLeftRadius: 16,
  },
  searchInputContainer: {
    height: 50,
    backgroundColor: COLORS.light,
    flex: 1,
    flexDirection: "row",
    alignItems: "center",
    paddingHorizontal: 20,
    borderRadius: 12,
  },
  sortBtn: {
    backgroundColor: COLORS.dark,
    height: 50,
    width: 50,
    borderRadius: 12,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 10,
  },
});
export default Search;
