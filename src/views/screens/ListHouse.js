import React, { useEffect, useState } from "react";
import { Button, FlatList, Image, Pressable, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import COLORS from "../../consts/colors";
import Icon from "react-native-vector-icons/FontAwesome5";
import category from "../../consts/categoryHome";
import Database from "../../services/db.service";
import { useIsFocused } from "@react-navigation/native";

const ListHouse = props => {
  const [selectedCategory, setSelectedCategory] = useState("all");
  const [houses, setHouses] = useState(props.route.params.route);
  const isFocused = useIsFocused();

  useEffect(() => {
   if(selectedCategory === "all") {
     Database.getListHouse().then(res => {
       console.log("res", res.reverse())
       if(res){
         setHouses(res.reverse());
       }
     }).catch(err => {
       console.log("err", err);
     });
   } else {
     Database.searchHouse("houseType", selectedCategory).then(res => {
       if(res){
         setHouses(res)
       }
     }).catch(err => {
       console.log("err", err);
     });
   }
  }, [selectedCategory, props.navigation, isFocused]);

  const renderItem = ({ item }) => {
    const selected = selectedCategory === item.id;
    return (
      <TouchableOpacity
        style={{
          backgroundColor: selected ? COLORS.blue : COLORS.light,
          padding: 4,
          borderRadius: 16,
          paddingVertical: 20,
          alignItems: "center",
          justifyContent: "center",
          marginRight: 12,
        }}
        onPress={() => {
          setSelectedCategory(item.id);
        }}
      >
        <View style={{
          width: 80,
          height: 68,
          borderRadius: 20,
          alignItems: "center",
          justifyContent: "center",
        }}>
          <Icon name={item.icon} size={24} color={selected ? COLORS.white : COLORS.primary} />
          <Text style={{
            marginTop: 4,
            fontWeight: "bold",
            color: selected ? COLORS.white : COLORS.primary,
          }}>{item.title}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  const renderHomeCard = ({ item }) => {
    console.log("house", item);
    return (
      <TouchableOpacity
        style={{
          marginBottom: 16,
          borderRadius: 16,
          backgroundColor: COLORS.white,
          marginHorizontal: 20,
          elevation: 10,
        }}
        onPress={() => {
          props.navigation.navigate("DetailScreen", item);
        }}
      >
        <View style={style.card}>
          <Image source={require("../../assets/images/house3.jpg")} style={style.cardImage} resizeMode="cover" />
          <View style={{ margin: 10, flex:1 }}>
            <Text style={{ fontSize: 17, fontWeight: "bold", color: COLORS.dark, marginBottom: 4 }}>{item.houseTitle}</Text>
            <View style={{ flexDirection: "row", marginBottom: 4 }}>
              <Text style={{ fontWeight: "bold", marginRight: 8, color: COLORS.primary }}>${item.housePrice}</Text>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Icon name="bed" size={14} color={COLORS.primary} style={{ marginRight: 3 }} />
                <Text style={{ color: COLORS.primary }}>{item.houseBedrooms}</Text>
              </View>
            </View>
            <View style={{ flexShrink: 1 }}>
              <Text numberOfLines={1}>{item.houseAddress}</Text>
            </View>
          </View>
          <View style={{ flexDirection: "column", alignItems: "center", justifyContent: "center", marginRight: 8}}>
            <Pressable style={{
              height: 30,
              width: 30,
              backgroundColor: COLORS.blue,
              borderRadius: 8,
              justifyContent: "center",
              alignItems: "center",
              paddingVertical: 4,
              paddingHorizontal: 4,
              marginBottom: 4
            }}>
              <Icon name="pen" size={12} color={COLORS.white}/>
            </Pressable>
            <Pressable style={{
              height: 30,
              width: 30,
              backgroundColor: COLORS.red,
              borderRadius: 8,
              justifyContent: "center",
              alignItems: "center",
              paddingVertical: 4,
              paddingHorizontal: 4,
            }}>
              <Icon name="trash" size={12} color={COLORS.white}/>
            </Pressable>
          </View>
        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={{ flex: 1, backgroundColor: COLORS.white }}>
      <View style={{ marginTop: 5, paddingHorizontal: 20 }}>
        <FlatList
          data={category}
          renderItem={renderItem}
          horizontal
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item) => item.id}
          contentContainerStyle={{ paddingVertical: 8, paddingHorizontal: 0 }}
        />
      </View>
      <View style={{ flex: 1 }}>
        <Text style={{ fontWeight: "bold", fontSize: 18, color: COLORS.dark, paddingHorizontal: 20 }}>Property List</Text>
        <FlatList
          data={houses}
          renderItem={renderHomeCard}
          keyExtractor={(item) => item.houseId}
          contentContainerStyle={{ paddingVertical: 8, paddingHorizontal: 4, marginBottom: 20 }}
          showsVerticalScrollIndicator={true}
        />
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  card: {
    height: 100,
    flexDirection: "row",
  },
  cardImage: {
    height: 100,
    width: "30%",
    borderTopLeftRadius: 16,
    borderBottomLeftRadius: 16,
  },
  spinnerTextStyle: {
    color: COLORS.white,
  },

});
export default ListHouse;
