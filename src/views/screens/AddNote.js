import React, { useEffect, useState } from "react";
import { Alert, Button, Dimensions, Pressable, StyleSheet, Text, TextInput, View } from "react-native";
import COLORS from "../../consts/colors";
import Spinner from "react-native-loading-spinner-overlay";
import { Controller, useForm } from "react-hook-form";
import Error from "../components/error";
import { Picker } from "@react-native-picker/picker";
import Database from "../../services/db.service";

const height = Dimensions.get("window").height;

const AddNote = props => {
  const [visible, setVisible] = useState(false);
  const { house } = props.route.params;
  const { control, register, setValue, handleSubmit, reset, formState: { errors } } = useForm({
    reValidateMode: "onChange",
    mode: "onChange",
    defaultValues: {
      properties: "",
      content: "",
    },
  });

  const onSubmit = (values) => {
    setVisible(true);
    Database.addNote({
      noteId: `${values.type}_${new Date().getTime()}`,
      houseId: house.houseId,
      houseType: values.properties.toString(),
      houseTitle: house.houseTitle,
      content: values.content,
      time: new Date().getTime(),
    }).then(res => {
      if (res) {
        setTimeout(() => {
          setVisible(false);
          Alert.alert(
            "Add note successfully!",
            "You have successfully added your note to the system.",
            [{ text: "OK", onPress: () => reset({}) }],
          );
        }, 2000);
      }
    }).catch(err => {
      setVisible(false);
      console.log("err", err);
    });
  };
  return (
    <View style={{ flex: 1, backgroundColor: COLORS.white }}>
      <Spinner
        visible={visible}
        textContent="Processing..."
        textStyle={{
          fontSize: 14,
          color: COLORS.white,
        }} />
      <View style={{ paddingVertical: 20, paddingHorizontal: 20, flex: 1 }}>
        <Controller
          control={control}
          render={({ field: { onChange, onBlur, value } }) => {
            return (
              <View>
                <Text style={styles.label}>Type</Text>
                <View style={styles.picker}>
                  <Picker
                    selectedValue={value}
                    onValueChange={itemValue => setValue("properties", itemValue)}
                    style={{ placeholderTextColor: "#fff" }}
                    label="Choose properties..."
                    {...register("properties", {
                      required: "Type is required.",
                    })}
                  >
                    <Picker.Item label="Type" value="House Type" style={{ fontSize: 14 }} />
                    <Picker.Item label="Price" value="House Price" style={{ fontSize: 14 }} />
                    <Picker.Item label="Address" value="House Address" style={{ fontSize: 14 }} />
                    <Picker.Item label="Bedrooms" value="House Bedrooms" style={{ fontSize: 14 }} />
                    <Picker.Item label="Furnished Type" value="House Furniture Types" style={{ fontSize: 14 }} />
                  </Picker>
                </View>
              </View>
            );
          }}
          name="properties"
          defaultValue=""
        />

        <Controller
          control={control}
          rules={{
            required: true,
          }}
          render={({ field: { onChange, onBlur, value } }) => (
            <View>
              <Text style={styles.label}>Title</Text>
              <TextInput
                placeholder="Input content..."
                multiline={true}
                numberOfLines={10}
                style={styles.input}
                onBlur={onBlur}
                onChangeText={onChange}
                value={value}
                {...register("content", {
                  required: "Content is required.",
                  minLength: {
                    value: 3,
                    message: "Content must be at least 4 characters.",
                  },
                })}
              />
            </View>
          )}
          name="content"
        />
        {errors.content && <Error>{errors.content.message}</Error>}
        <View style={{
          flex: 1,
          justifyContent: "flex-end",
          bottom: 10,
        }}>
          <Button color={COLORS.blue} title="Submit" onPress={handleSubmit(onSubmit)} />
        </View>
        <View>
        </View>
      </View>
    </View>
  );
};
const styles = StyleSheet.create({
  picker: {
    justifyContent: "center",
    height: 45,
    backgroundColor: COLORS.light,
    marginBottom: 5,
    borderRadius: 6,
  },
  label: {
    color: COLORS.dark,
    fontWeight: "bold",
    marginBottom: 5,
    opacity: 0.7,
  },
  input: {
    justifyContent: "flex-start",
    paddingHorizontal: 15,
    height: 100,
    marginBottom: 5,
    backgroundColor: COLORS.light,
    borderRadius: 8,
  },
});
export default AddNote;
