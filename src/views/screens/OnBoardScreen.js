import React from "react";
import { StyleSheet, Image, SafeAreaView, Text, View, StatusBar, Pressable } from "react-native";
import COLORS from "../../consts/colors";

const OnBoardScreen = props => {
  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.white }}>
      <StatusBar translucent backgroundColor={COLORS.tranparent} />
      <Image style={style.image} source={require("../../assets/images/house1.jpg")} />
      <View style={style.indicatorContainer}>
        <View style={style.indicator}></View>
        <View style={style.indicator}></View>
        <View style={[style.indicator, style.indicatorActive]}></View>
      </View>
      <View style={{ paddingHorizontal: 20, marginTop: 20 }}>
        <View>
          <Text style={style.title}>Welcome</Text>
          <Text style={style.title}>to RentalZ App</Text>
        </View>
        <View style={{ marginTop: 10 }}>
          <Text style={style.text}>Find your sweet home</Text>
          <Text style={style.text}>visits in just few clicks</Text>
        </View>
      </View>
      <View style={{
        flex: 1,
        justifyContent: "flex-end",
        bottom: 40,
      }}>
        <Pressable onPress={() => {
          props.navigation.navigate("Login");
        }}>
          <View style={style.btn}>
            <Text style={style.btnText}>Get Started</Text>
          </View>
        </Pressable>
      </View>
    </SafeAreaView>
  );
};
const style = StyleSheet.create({
  image: {
    height: 420,
    width: "100%",
    borderBottomLeftRadius: 100,
  },
  indicatorContainer: {
    height: 20,
    display: "flex",
    flexDirection: "row",
    alignItems: "center",
    justifyContent: "center",
  },
  indicator: {
    height: 3,
    width: 30,
    backgroundColor: COLORS.grey,
    marginHorizontal: 5,
    borderRadius: 5,
  },
  indicatorActive: {
    backgroundColor: COLORS.dark,
  },
  title: {
    fontSize: 32,
    fontWeight: "bold",
    color: COLORS.dark,
  },
  text: {
    fontSize: 16,
    color: COLORS.grey,
  },
  btnText: {
    color: COLORS.white,
  },
  btn: {
    backgroundColor: COLORS.dark,
    height: 60,
    borderRadius: 15,
    alignItems: "center",
    justifyContent: "center",
    marginHorizontal: 20,
  },
});
export default OnBoardScreen;
