import React, { useEffect, useState } from "react";
import {
  Alert,
  Dimensions,
  FlatList,
  Image,
  ImageBackground, Pressable,
  SafeAreaView,
  ScrollView,
  StyleSheet,
  Text,
  View,
} from "react-native";
import COLORS from "../../consts/colors";
import Icon from "react-native-vector-icons/FontAwesome5";
import IconAnt from "react-native-vector-icons/AntDesign";
import Database from "../../services/db.service";

const { width } = Dimensions.get("screen");
const DetailHome = props => {
  const [house, setHouse] = useState(props.route.params);
  const [isHeart, setIsHeart] = useState(false);

  const InteriorCard = ({ interior }) => {
    return <Image source={interior} style={style.interiorImage} />;
  };

  useEffect(() => {
    if (props.route.params.mode === "reRender") {
      // Get information House
      Database.getHouseById(props.route.params.houseId).then(res => {
        console.log("res", res);
        if (res) {
          setHouse(res);
        }
      }).catch(err => {
        console.log("err", err);
      });
    }
  }, [props.route.params]);


  return (
    <SafeAreaView style={{ flex: 1, backgroundColor: COLORS.white }}>
      <ScrollView>
        <View>
          <ImageBackground style={style.imageBackground} source={require("../../assets/images/house2.jpg")}>
            <View style={style.header}>
              <View style={style.btn}>
                {isHeart ? (
                  <IconAnt name="heart" color={COLORS.red} style={{ index: 10 }} size={20}
                           onPress={() => {
                             setIsHeart(!isHeart);
                           }} />
                ) : (
                  <Icon name="heart" style={{ index: 10 }} size={20}
                        onPress={() => {
                          setIsHeart(!isHeart);
                        }} />
                )}
              </View>
            </View>
          </ImageBackground>
        </View>
        <View style={{ padding: 20 }}>
          <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
            <Text style={{ fontSize: 20, fontWeight: "bold" }}>
              {house.houseTitle}
            </Text>
            <View style={{ flexDirection: "row", alignItems: "center" }}>
              <Pressable
                style={{
                  height: 30,
                  width: 30,
                  backgroundColor: COLORS.green,
                  borderRadius: 8,
                  justifyContent: "center",
                  alignItems: "center",
                  paddingVertical: 4,
                  paddingHorizontal: 4,
                  marginRight: 8,
                }}
                onPress={() => {
                  props.navigation.navigate("AddNote", {
                    house: house,
                  });
                }}
              >
                <Icon name="list-ul" size={12} color={COLORS.white} />
              </Pressable>
              <Pressable
                style={{
                  height: 30,
                  width: 30,
                  backgroundColor: COLORS.blue,
                  borderRadius: 8,
                  justifyContent: "center",
                  alignItems: "center",
                  paddingVertical: 4,
                  paddingHorizontal: 4,
                  marginRight: 8,
                }}
                onPress={() => {
                  props.navigation.navigate("AddProduct", {
                    house: house,
                    mode: "edit",
                  });
                }}
              >
                <Icon name="pen" size={12} color={COLORS.white} />
              </Pressable>
              <Pressable
                style={{
                  height: 30,
                  width: 30,
                  backgroundColor: COLORS.red,
                  borderRadius: 8,
                  justifyContent: "center",
                  alignItems: "center",
                  paddingVertical: 4,
                  paddingHorizontal: 4,
                }}
                onPress={() => {
                  Alert.alert(
                    "Are you sure to delete this property?!",
                    "This action can not undo, so do you want to delete?",
                    [
                      {
                        text: "Cancel", onPress: () => console.log("cancel"), style: "cancel",
                      }, {
                      text: "OK", onPress: () => {
                        Database.deleteHouse(house.houseId).then(res => {
                          if (res) {
                            Alert.alert(
                              "Delete successfully!",
                              "You have deleted this information from the application!",
                              [{ text: "OK", onPress: () => props.navigation.navigate("Overview") }],
                            );
                          }
                        }).catch(err => {
                          console.log("err", err);
                        });
                      },
                    }],
                  );
                }}
              >
                <Icon name="trash" size={12} color={COLORS.white} />
              </Pressable>
            </View>
          </View>

          <Text style={{ fontSize: 16, color: COLORS.grey }}>
            {house.houseAddress}
          </Text>
          {/* Facilities container */}
          <View style={{ flexDirection: "row", marginTop: 20 }}>
            <View style={style.facility}>
              <Icon name="crown" size={16} />
              <Text style={style.facilityText}>{house.houseType}</Text>
            </View>
            <View style={style.facility}>
              <Icon name="bed" size={16} />
              <Text style={style.facilityText}>{house.houseBedrooms}</Text>
            </View>
            <View style={style.facility}>
              <Icon name="door-closed" size={16} />
              <Text style={style.facilityText}>{house?.houseFurnitureTypes ?? "No data"}</Text>
            </View>
          </View>
          <Text style={{ marginTop: 20, color: COLORS.grey }}>
            {house.houseNote}
          </Text>
          <View style={{ marginTop: 20 }}>
            <View style={{ ...style.facility, marginBottom: 5 }}>
              <Text style={style.facilityText}>Reporter: {house.houseReporterName}</Text>
            </View>
            <View style={{ flexDirection: "row" }}>
              <View style={style.facility}>
                <Icon name="phone" size={14} />
                <Text style={style.facilityText}>{house.phoneNumber}</Text>
              </View>
              <View style={style.facility}>
                <Icon name="mail-bulk" size={14} />
                <Text style={style.facilityText}>{house.email}</Text>
              </View>
            </View>
          </View>

          {/* Interior list */}
          <FlatList
            contentContainerStyle={{ marginTop: 20 }}
            horizontal
            showsHorizontalScrollIndicator={false}
            keyExtractor={(_, key) => key.toString()}
            data={[
              require("../../assets/images/interior1.jpg"),
              require("../../assets/images/interior2.jpg"),
              require("../../assets/images/interior3.jpg"),
            ]}
            renderItem={({ item }) => <InteriorCard interior={item} />}
          />
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

const style = StyleSheet.create({
  imageBackground: {
    height: 350,
  },
  header: {
    flexDirection: "row",
    justifyContent: "flex-end",
    paddingVertical: 20,
    paddingHorizontal: 20,
  },
  btn: {
    height: 40,
    width: 40,
    backgroundColor: COLORS.white,
    borderRadius: 8,
    alignItems: "center",
    justifyContent: "center",
  },
  backgroundImageContainer: {
    elevation: 20,
    marginHorizontal: 20,
    marginTop: 20,
    alignItems: "center",
    height: 350,
  },
  backgroundImage: {
    height: "100%",
    width: "100%",
    borderRadius: 20,
    overflow: "hidden",
  },
  headerBtn: {
    height: 50,
    width: 50,
    backgroundColor: COLORS.white,
    borderRadius: 10,
    justifyContent: "center",
    alignItems: "center",
  },
  ratingTag: {
    height: 30,
    width: 35,
    backgroundColor: COLORS.blue,
    borderRadius: 5,
    justifyContent: "center",
    alignItems: "center",
  },
  virtualTag: {
    top: -20,
    width: 120,
    borderRadius: 10,
    height: 40,
    paddingHorizontal: 20,
    backgroundColor: COLORS.dark,
    justifyContent: "center",
    alignItems: "center",
  },
  interiorImage: {
    width: width / 3 - 20,
    height: 80,
    marginRight: 10,
    borderRadius: 10,
  },
  footer: {
    height: 70,
    backgroundColor: COLORS.light,
    borderRadius: 10,
    paddingHorizontal: 20,
    alignItems: "center",
    flexDirection: "row",
    justifyContent: "space-between",
    marginVertical: 10,
  },
  bookNowBtn: {
    height: 50,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: COLORS.dark,
    borderRadius: 10,
    paddingHorizontal: 20,
  },
  detailsContainer: { flex: 1, paddingHorizontal: 20, marginTop: 40 },
  facility: { flexDirection: "row", marginRight: 20 },
  facilityText: { marginLeft: 5, color: COLORS.grey },
});
export default DetailHome;
