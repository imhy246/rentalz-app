import React, { useEffect, useState } from "react";
import {
  Dimensions,
  FlatList,
  Image,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import COLORS from "../../consts/colors";
import category from "../../consts/categoryHome";
import Icon from "react-native-vector-icons/FontAwesome5";
import Database from "../../services/db.service";
import { useIsFocused } from "@react-navigation/native";
const height = Dimensions.get("window").height;
const Home = props => {
  const [selectedCategory, setSelectedCategory] = useState(0);
  const [houses, setHouses] = useState([]);
  const [recommend, setRecommend] = useState([]);
  const isFocused = useIsFocused();

  useEffect(() => {
    Database.getListHouse().then(res => {
      console.log("res", res.reverse())
      if(res){
        setHouses(res.reverse());
        setRecommend(res.reverse().slice(0,5));
      }
    }).catch(err => {
      console.log("err", err);
    });
  }, [isFocused]);

  const renderItem = ({ item }) => {
    const selected = selectedCategory === item.id;
    return (
      <TouchableOpacity
        style={{
          backgroundColor: selected ? COLORS.primary : COLORS.light,
          padding: 4,
          borderRadius: 16,
          paddingVertical: 20,
          alignItems: "center",
          justifyContent: "center",
          marginRight: 12,
        }}
        onPress={() => {
          setSelectedCategory(item.id);
        }}
      >
        <View style={{
          width: 80,
          height: 68,
          borderRadius: 20,
          alignItems: "center",
          justifyContent: "center",
        }}>
          <Icon name={item.icon} size={24} color={selected ? COLORS.white : COLORS.primary} />
          <Text style={{
            marginTop: 4,
            fontWeight: "bold",
            color: selected ? COLORS.white : COLORS.primary,
          }}>{item.title}</Text>
        </View>
      </TouchableOpacity>
    );
  };

  const renderHouseItem = ({ item }) => {
    return (
      <TouchableOpacity
        style={{
        marginBottom: 16,
        borderRadius: 16,
        backgroundColor: COLORS.white,
        elevation: 4,
      }}
        onPress={() => {
          props.navigation.navigate("DetailScreen", item)
        }}
      >
        <View style={style.card}>
          <Image source={require('../../assets/images/house1.jpg')} style={style.cardImage} resizeMode="cover" />
          <View style={{
            paddingHorizontal:16,
            paddingBottom: 8
          }}>
            <View style={{
              flexDirection: "row",
              alignItems: "center",
              marginTop: 8,
            }}>
              <Icon name="home" size={14} style={{ marginRight: 4}} color={COLORS.primary}  />
              <Text style={{ textTransform: "capitalize", fontWeight: "500", color: COLORS.primary }}>{item.houseType}</Text>
            </View>
            <View style={{
              flexDirection: "row",
              justifyContent: "space-between",
              marginTop: 4,
            }}>
              <Text style={{ fontWeight: "bold", fontSize: 18, color: COLORS.primary }}>{item.houseTitle}</Text>
              <Text style={{ fontWeight: "bold", color: COLORS.primary, fontSize: 16 }}>${item.housePrice}</Text>
            </View>
            <View style={{
              flexDirection: "row",
              alignItems: "center",
            }}>
              <Text style={{
                textTransform: "capitalize",
              }}>{item.houseAddress}</Text>
            </View>
          </View>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <View style={{ flex: 1, backgroundColor: "white", paddingHorizontal: 20 }}>
      <View style={style.header}>
        <View>
          <Text style={style.text}>Find your</Text>
          <Text style={style.label}>Perfect Home</Text>
        </View>
        <Image source={require("../../assets/images/person.jpg")} style={style.profileImage} />
      </View>
      <View>
        <FlatList
          data={category}
          renderItem={renderItem}
          horizontal
          showsHorizontalScrollIndicator={false}
          keyExtractor={(item) => item.id}
          contentContainerStyle={{ paddingVertical: 8,  paddingHorizontal: 4 }}
        />
      </View>
      <View style={{flex: 1, marginBottom: 8}}>
       <View style={{marginBottom: 8, flexDirection: "row", justifyContent: "space-between"}}>
         <Text style={{ fontWeight: "bold", fontSize: 18, color: COLORS.dark}}>Recommend</Text>
         <Pressable onPress={() => {
           props.navigation.navigate('HouseList', {
             route: houses
           })
         }}>
           <Text style={{ fontWeight: "bold", fontSize: 15, color: COLORS.dark}}>See All</Text>
         </Pressable>
       </View>
        <FlatList
          data={recommend}
          renderItem={renderHouseItem}
          keyExtractor={item => item.houseId}
          contentContainerStyle={{ paddingVertical: 8, paddingHorizontal: 4 }}
        />
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  header: {
    paddingVertical: 20,
    flexDirection: "row",
    justifyContent: "space-between",
    alignItems: "center",
  },
  profileImage: {
    height: 50,
    width: 50,
    borderRadius: 50,
  },
  text: {
    fontSize: 24,
    color: COLORS.grey,
  },
  label: {
    fontSize: 24,
    color: COLORS.dark,
    fontWeight: "bold",
  },
  card: {
    height: 220,
    borderRadius: 16,
  },
  cardImage: {
    width: "100%",
    height: 120,
    borderTopLeftRadius: 15,
    borderTopRightRadius: 15,
  },
});
export default Home;
