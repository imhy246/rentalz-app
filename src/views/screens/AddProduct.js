import {
  Alert,
  Button,
  Dimensions,
  ImageBackground,
  ScrollView,
  StyleSheet,
  Text,
  TextInput,
  TouchableOpacity,
  View,
} from "react-native";
import { Controller, useForm } from "react-hook-form";
import React, { useEffect, useState } from "react";
import COLORS from "../../consts/colors";
import Icon from "react-native-vector-icons/FontAwesome";
import Error from "../components/error";
import Validation from "../../utils/ValidationAddProduct";
import { Picker } from "@react-native-picker/picker";
import DateTimePickerModal from "react-native-modal-datetime-picker";
import Database from "../../services/db.service";
import moment from "moment";
import Spinner from "react-native-loading-spinner-overlay";

const height = Dimensions.get("window").height;

const AddProduct = props => {
  const [date, setDate] = useState(moment(new Date()).format("DD-MM-YYYY"));
  const [dateCurrent, setDateCurrent] = useState(new Date());
  const [isDatePickerVisible, setDatePickerVisibility] = useState(false);
  const [visible, setVisible] = useState(false);
  const { control, register, setValue, getValues, handleSubmit, reset, formState: { errors } } = useForm({
    reValidateMode: "onChange",
    mode: "onChange",
    defaultValues: {
      houseTitle: "",
      houseType: "",
      date: new Date(),
      houseAddress: "",
      houseBedrooms: "",
      housePrice: "",
      houseFurnitureTypes: "",
      houseNote: "",
      reporterName: "",
      email: "",
      phoneNumber: "",
    },
  });

  const showDatePicker = () => {
    setDatePickerVisibility(true);
  };

  const hideDatePicker = () => {
    setDatePickerVisibility(false);
  };


  const handleConfirm = date => {
    const currentDate = date || currentDate;
    let newDate = moment(new Date(currentDate)).format("DD-MM-YYYY");
    hideDatePicker();
    setDate(newDate);
    setDateCurrent(currentDate);
  };

  useEffect(() => {
    console.log("dsfkhsd", props.route.params)
  })

  const onSubmit = data => {
    setVisible(true);
    //Add
    console.log("update")
   if(!props.route.params) {
     Database.addHouse({ ...data, houseId: new Date().getTime() }).then(res => {
       console.log("res", res);
       if (res) {
         setTimeout(() => {
           setVisible(false);
           Alert.alert(
             "Add property successfully!",
             "You have successfully added your post to the system.",
             [{ text: "OK", onPress: () => reset({}) }],
           );
         }, 2000);
       }
     }).catch(err => {
       console.log("err", err);
       setVisible(false);
       Alert.alert(
         "Error!",
         "An error occurred during the add. Please try again later",
         [{ text: "OK", onPress: () => {console.log("Error!")}}],
       );
     });
   } else {
     // Update
     console.log("update")
     Database.updateHouse(props.route.params.house.houseId, { ...data }).then(res => {
       console.log("resfdfff", res.rows);
       if (res) {
         setTimeout(() => {
           setVisible(false);
           Alert.alert(
             "Update property successfully!",
             "You have successfully update your post to the system.",
             [{ text: "OK", onPress: () => props.navigation.navigate("DetailScreen", {houseId: props.route.params.house.houseId, mode: "reRender"})}],
           );
         }, 2000);
       }
     }).catch(err => {
       console.log("err", err);
       setVisible(false);
       Alert.alert(
         "Error!",
         "An error occurred during the add. Please try again later",
         [{ text: "OK", onPress: () => {console.log("Error!")}}],
       );
     });
   }
  };


  useEffect(() => {
    if(props.route.params){
      const {house, mode} = props.route.params
      if(house && mode == "edit"){
        setValue("houseTitle", house.houseTitle, {shouldValidate: false})
        setValue("houseType", house.houseType)
        setValue("houseAddress", house.houseAddress)
        setValue("houseBedrooms", house.houseBedrooms)
        setValue("houseFurnitureTypes", house.houseFurnitureTypes)
        setValue("houseNote", house.houseNote)
        setValue("housePrice", house.housePrice)
        setValue("reporterName", house.houseReporterName)
        setValue("phoneNumber", house.phoneNumber)
        setValue("email", house.email)
      }
    }
  }, [])

  useEffect(() => {
    //Get list

    // Database.getListHouse().then(res => {
    //   console.log("res", res)
    // }).catch(err => {
    //   console.log("err", err)
    // })

    // Get information House
    // Database.getHouseById(1637081204208).then(res => {
    //   console.log("res", res)
    // }).catch(err => {
    //   console.log("err", err)
    // })

    // Database.deleteHouse(1637081204208).then(res => {
    //   console.log("res", res);
    // }).catch(err => {
    //   console.log("err", err);
    // });

    //  search
    // Database.searchHouse("houseType", "room").then(res => {
    //   console.log("res", res);
    // }).catch(err => {
    //   console.log("err", err);
    // });
  });

  return (
    <View style={{ flex: 1, backgroundColor: COLORS.white }}>
      <Spinner
        visible={visible}
        textContent="Processing..."
        textStyle={{
          fontSize: 14,
          color: COLORS.white,
        }}/>

      <ImageBackground style={styles.image} source={require("../../assets/images/house3.jpg")} />
      <View style={styles.main}>
        <View>
          <View style={{
            alignItems: "center",
          }}>
            <View style={{
              backgroundColor: COLORS.blue,
              height: 70,
              width: 70,
              borderRadius: 50,
              justifyContent: "center",
              alignItems: "center",
              marginTop: -30,
              opacity: 1,
              zIndex: -1,
            }}>
              <Icon name="home" size={24} color={COLORS.white} style={{
                margin: "auto",
                zIndex: 10,
              }} />
            </View>
          </View>
        </View>
        <View>
          <ScrollView>
            <View>
              <View>
                {/*Title Input*/}
                <Controller
                  control={control}
                  rules={{
                    required: true,
                  }}
                  render={({ field: { onChange, onBlur, value } }) => (
                    <View>
                      <Text style={styles.label}>Title</Text>
                      <TextInput
                        placeholder="Input title..."
                        style={styles.input}
                        onBlur={onBlur}
                        onChangeText={onChange}
                        value={value}
                        {...register("houseTitle", {
                          required: "Title is required.",
                          minLength: {
                            value: 4,
                            message: "Title must be at least 4 characters.",
                          },
                        })}
                      />
                    </View>
                  )}
                  name="houseTitle"
                />
                {errors.houseTitle && <Error>{errors.houseTitle.message}</Error>}

                <Controller
                  control={control}
                  render={({ field: { onChange, onBlur, value } }) => {
                    return (
                      <View>
                        <Text style={styles.label}>Type</Text>
                        <View style={styles.picker}>
                          <Picker
                            selectedValue={value}
                            onValueChange={itemValue => setValue("houseType", itemValue)}
                            style={{ placeholderTextColor: "#fff" }}
                            label="Choose type..."
                            {...register("houseType", {
                              required: "Type is required.",
                            })}
                          >
                            <Picker.Item label="Room" value="room" style={{ fontSize: 14 }} />
                            <Picker.Item label="House" value="house" style={{ fontSize: 14 }} />
                            <Picker.Item label="Flat" value="flat" style={{ fontSize: 14 }} />
                          </Picker>
                        </View>
                      </View>
                    );
                  }}
                  name="houseType"
                  defaultValue=""
                />
                {errors.houseType && <Error>{errors.houseType.message}</Error>}

                <Controller
                  render={({ field: { value, onChange, onBlur } }) => {
                    return (
                      <View>
                        <Text style={styles.label}>Date</Text>
                        <TouchableOpacity onPress={showDatePicker} style={{
                          paddingHorizontal: 15,
                          height: 45,
                          marginBottom: 5,
                          backgroundColor: COLORS.light,
                          borderRadius: 8,
                          color: COLORS.dark,
                          justifyContent: "center",
                        }}>
                          <Text>{date ?? "Choose date..."}</Text>
                        </TouchableOpacity>
                      </View>
                    );
                  }}
                  name="dateTime"
                  control={control}
                />

                <DateTimePickerModal
                  isVisible={isDatePickerVisible}
                  mode="date"
                  onConfirm={handleConfirm}
                  onCancel={hideDatePicker}
                  date={dateCurrent}
                />

                {/* Address input*/}
                <Controller
                  control={control}
                  rules={{
                    required: true,
                  }}
                  render={({ field: { onChange, onBlur, value } }) => (
                    <View>
                      <Text style={styles.label}>Address</Text>
                      <TextInput
                        placeholder="Input address..."
                        style={styles.input}
                        onBlur={onBlur}
                        onChangeText={onChange}
                        value={value}
                        {...register("houseAddress", {
                          required: "Address is required.",
                          maxLength: {
                            value: 255,
                            message: "Please entered less then 255 characters.",
                          },
                        })}
                      />
                    </View>
                  )}
                  name="houseAddress"
                />
                {errors.houseAddress && <Error>This is required.</Error>}

                {/* Amount of room input*/}
                <Controller
                  control={control}
                  rules={{
                    required: true,
                  }}
                  render={({ field: { onChange, onBlur, value } }) => (
                    <View>
                      <Text style={styles.label}>Bedrooms</Text>
                      <TextInput
                        placeholder="Input amount of room..."
                        style={styles.input}
                        onBlur={onBlur}
                        onChangeText={onChange}
                        value={value}
                        {...register("houseBedrooms", {
                          required: "Bedroom is required.",
                        })}
                      />
                    </View>
                  )}
                  name="houseBedrooms"
                />
                {errors.houseBedrooms && <Error>{errors.houseBedrooms.message}</Error>}

                <Controller
                  control={control}
                  render={({ field: { onChange, onBlur, value } }) => {
                    console.log("check furnished", value);
                    return (
                      <View>
                        <Text style={styles.label}>Furniture types</Text>
                        <View style={styles.picker}>
                          <Picker
                            selectedValue={value}
                            onValueChange={itemValue => {
                              setValue("houseFurnitureTypes", itemValue);
                            }}
                          >
                            <Picker.Item label="Furnished" value="furnished" style={{ fontSize: 14 }} />
                            <Picker.Item label="Unfurnished" value="unFurnished" style={{ fontSize: 14 }} />
                            <Picker.Item label="Part Furnished" value="partFurnished" style={{ fontSize: 14 }} />
                          </Picker>
                        </View>
                      </View>
                    );
                  }}
                  name="houseFurnitureTypes"
                  defaultValue="furnished"
                />
                {errors.houseFurnitureTypes && <Error>{errors.houseFurnitureTypes.message}</Error>}

                {/* Price input*/}
                <Controller
                  control={control}
                  rules={{
                    required: true,
                  }}
                  render={({ field: { onChange, onBlur, value } }) => (
                    <View>
                      <Text style={styles.label}>Price (USD)</Text>
                      <TextInput
                        placeholder="Input price..."
                        style={styles.input}
                        onBlur={onBlur}
                        onChangeText={onChange}
                        value={value}
                        {...register("housePrice", {
                          required: "Price is required.",
                          validate: {
                            isValidPrice: value => Validation.isValidPrice(value) || "Please input valid price.",
                          },
                        })}
                      />
                    </View>
                  )}
                  name="housePrice"
                />
                {errors.housePrice && <Error>{errors.housePrice.message}</Error>}

                {/* Description input*/}
                <Controller
                  control={control}
                  rules={{
                    required: true,
                  }}
                  render={({ field: { onChange, onBlur, value } }) => (
                    <View>
                      <Text style={styles.label}>Note</Text>
                      <TextInput
                        placeholder="Input note..."
                        style={{ ...styles.input }}
                        onBlur={onBlur}
                        onChangeText={onChange}
                        value={value}
                        {...register("houseNote", {
                          maxLength: {
                            value: 500,
                            message: "Please entered less then 255 characters.",
                          },
                        })}
                      />
                    </View>
                  )}
                  name="houseNote"
                />
                {errors.houseNote && <Error>{errors.houseNote.message}</Error>}

                {/* Phone number input*/}
                <Controller
                  control={control}
                  rules={{
                    required: true,
                  }}
                  render={({ field: { onChange, onBlur, value } }) => (
                    <View>
                      <Text style={styles.label}>Reporter Name</Text>
                      <TextInput
                        placeholder="Input name of reporter..."
                        style={styles.input}
                        onBlur={onBlur}
                        onChangeText={onChange}
                        value={value}
                        {...register("reporterName", {
                          required: "Name reporter is required.",
                          minLength: {
                            value: 2,
                            message: "Reporter name be at least 2 characters.",
                          },
                        })}
                      />
                    </View>
                  )}
                  name="reporterName"
                />
                {errors.reporterName && <Error>{errors.reporterName.message}</Error>}

                {/* Phone number input*/}
                <Controller
                  control={control}
                  rules={{
                    required: true,
                  }}
                  render={({ field: { onChange, onBlur, value } }) => (
                    <View>
                      <Text style={styles.label}>Phone Number</Text>
                      <TextInput
                        placeholder="Input phone number..."
                        style={styles.input}
                        onBlur={onBlur}
                        onChangeText={onChange}
                        value={value}
                        {...register("phoneNumber", {
                          required: "Phone is required.",
                          validate: {
                            isValidNumber: value => Validation.isValidPhone(value) || "Please input valid number.",
                          },
                        })}
                      />
                    </View>
                  )}
                  name="phoneNumber"
                />
                {errors.phoneNumber && <Error>{errors.phoneNumber.message}</Error>}

                {/* Email input*/}
                <Controller
                  control={control}
                  rules={{
                    required: true,
                  }}
                  render={({ field: { onChange, onBlur, value } }) => (
                    <View>
                      <Text style={styles.label}>Email</Text>
                      <TextInput
                        placeholder="Input email..."
                        style={{ ...styles.input, marginBottom: 10 }}
                        onBlur={onBlur}
                        onChangeText={onChange}
                        value={value}
                        {...register("email", {
                          required: "Email is required.",
                          validate: {
                            isEmail: value => Validation.validationEmail(value) || "Please enter your email in format, like rentalZ@example.com",
                          },
                        })}
                      />
                    </View>
                  )}
                  name="email"
                />
                {errors.email && <Error>{errors.email.message}</Error>}

                <Button title="Submit" color={COLORS.blue} onPress={handleSubmit(onSubmit)} />
              </View>
            </View>
          </ScrollView>
        </View>
      </View>
    </View>

  );
};

const styles = StyleSheet.create({
  picker: {
    justifyContent: "center",
    height: 45,
    backgroundColor: COLORS.light,
    marginBottom: 5,
    borderRadius: 6,
  },
  label: {
    color: COLORS.dark,
    fontWeight: "bold",
    marginBottom: 5,
    opacity: 0.7,
  },
  image: {
    height: height * 0.3,
    width: "100%",
  },
  input: {
    paddingHorizontal: 15,
    height: 45,
    marginBottom: 5,
    backgroundColor: COLORS.light,
    borderRadius: 8,
  },
  header: {
    flexDirection: "row",
    justifyContent: "space-between",
    paddingVertical: 10,
  },
  btn: {
    height: 40,
    width: 40,
    backgroundColor: COLORS.white,
    borderRadius: 8,
    alignItems: "center",
    justifyContent: "center",
  },
  main: {
    flex: 1,
    backgroundColor: COLORS.white,
    marginTop: -(height * 0.2),
    borderRadius: 30,
    paddingHorizontal: 40,
    marginBottom: (height * 0.08),
  },
  spinnerTextStyle: {
    color: COLORS.white,
  },
});

export default AddProduct;
