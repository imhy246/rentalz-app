import React from "react";
import { Image, ImageBackground, Text, TouchableOpacity, View } from "react-native";
import COLORS from "../../consts/colors";
import Spinner from "react-native-loading-spinner-overlay";
import IconAnt from "react-native-vector-icons/AntDesign";

const SettingAccount = props => {
  return (
    <View style={{ flex: 1, backgroundColor: COLORS.white }}>
      <View style={{alignItems: "center", marginTop: 15}}>
        <Image source={require("../../assets/images/person.jpg")}
               style={{
                 borderColor: COLORS.white,
                 borderWidth: 2,
                 height: 80,
                 width: 80,
                 borderRadius: 50,
               }} />
        <Text style={{color: COLORS.dark, fontWeight: "bold", marginTop: 10}}>Nguyen Thu Thuy</Text>
      </View>
      <View style={{paddingHorizontal: 40, marginTop: 20}}>
        <TouchableOpacity
          style={{
            width: "100%",
            height: 40,
            borderRadius: 8,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: COLORS.white,
            elevation: 4,
            marginBottom: 15
          }}
        >
          <Text style={{color: COLORS.dark, fontWeight: "400"}}>Edit information</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            width: "100%",
            height: 40,
            borderRadius: 8,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: COLORS.white,
            elevation: 4,
            marginBottom: 15
          }}
        >
          <Text style={{color: COLORS.dark, fontWeight: "400"}}>Change avatar</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            width: "100%",
            height: 40,
            borderRadius: 8,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: COLORS.white,
            elevation: 4,
            marginBottom: 15
          }}
        >
          <Text style={{color: COLORS.dark, fontWeight: "400"}}>Change password</Text>
        </TouchableOpacity>
        <TouchableOpacity
          style={{
            width: "100%",
            height: 40,
            borderRadius: 8,
            alignItems: "center",
            justifyContent: "center",
            backgroundColor: COLORS.white,
            elevation: 4,
            marginBottom: 15
          }}
        >
          <Text style={{color: COLORS.blue, fontWeight: "400"}}>Logout</Text>
        </TouchableOpacity>
      </View>
    </View>
  );
};
export default SettingAccount;
