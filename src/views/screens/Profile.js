import React, { useEffect, useState } from "react";
import {
  Alert,
  Dimensions,
  FlatList,
  Image,
  ImageBackground,
  Pressable,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from "react-native";
import COLORS from "../../consts/colors";
import Database from "../../services/db.service";
import Icon from "react-native-vector-icons/FontAwesome5";
import IconAnt from "react-native-vector-icons/AntDesign";
import Spinner from "react-native-loading-spinner-overlay";
import { Picker } from "@react-native-picker/picker";
import { useIsFocused } from "@react-navigation/native";

const height = Dimensions.get("window").height;
const Profile = props => {
  const [notes, setNotes] = useState([]);
  const [selectedNoteType, setSelectedNoteType] = useState("all");
  const [visible, setVisible] = useState(false);
  const isFocused = useIsFocused();


  useEffect(() => {
    setVisible(true);
    if (selectedNoteType && selectedNoteType === "all") {
      Database.getNoteList().then(res => {
        console.log("res", res);
        if (res) {
          setNotes(res);
          setVisible(false);
        }
      }).catch(err => {
        setVisible(false);
        console.log("err", err);
      });
    } else {
      Database.searchNote(selectedNoteType).then(res => {
        setVisible(false);
        if (res) {
          setNotes(res);
        }
      }).catch(err => {
        setVisible(false);
        console.log("err", err);
      });
    }
  }, [selectedNoteType, isFocused]);

  const renderNoteCard = ({ item }) => (
    <TouchableOpacity
      style={{
        marginBottom: 16,
        borderRadius: 16,
        backgroundColor: COLORS.white,
        marginHorizontal: 20,
        elevation: 10,
      }}
      onPress={() => {
      }}
    >
      <View style={styles.card}>
        <View style={{ margin: 10, flex: 1 }}>
          <Text
            style={{ fontSize: 17, fontWeight: "bold", color: COLORS.dark, marginBottom: 4 }}>{item.houseTitle}</Text>
          <View style={{ flexDirection: "row", marginBottom: 4 }}>
            <Text style={{ fontWeight: "bold", marginRight: 8, color: COLORS.primary }}>{item.houseType}</Text>
          </View>
          <View style={{ flexShrink: 1 }}>
            <Text numberOfLines={1}>{item.content}</Text>
          </View>
        </View>
        <View style={{ flexDirection: "column", alignItems: "center", justifyContent: "center", marginRight: 8 }}>
          <Pressable style={{
            height: 30,
            width: 30,
            backgroundColor: COLORS.blue,
            borderRadius: 8,
            justifyContent: "center",
            alignItems: "center",
            paddingVertical: 4,
            paddingHorizontal: 4,
            marginBottom: 4,
          }}>
            <Icon name="pen" size={12} color={COLORS.white} />
          </Pressable>
          <Pressable
            style={{
              height: 30,
              width: 30,
              backgroundColor: COLORS.red,
              borderRadius: 8,
              justifyContent: "center",
              alignItems: "center",
              paddingVertical: 4,
              paddingHorizontal: 4,
            }}
            onPress={() => {
              Alert.alert(
                "Are you sure to delete this note?!",
                "This action can not undo, so do you want to delete?",
                [{
                  text: "OK",
                  onPress: () => {
                    Database.deleteNote(item.noteId).then(res => {
                      if (res.rowsAffected !== 0) {
                        const newNotes = notes?.filter(note => note.id !== item.noteId);
                        setNotes(newNotes);
                        Alert.alert(
                          "Delete note successfully!",
                          "This record has been remove from application.",
                          [{
                            text: "OK",
                            onPress: () => {
                              console.log("delete note successfully!")
                            },
                          }]);
                      }
                    }).catch(err => {
                      console.log("err", err);
                    });
                  },
                }, {
                  text: "Cancel",
                  onPress: () => console.log("Cancel Pressed"),
                  style: "cancel",
                }],
              );
            }}
          >
            <Icon name="trash" size={12} color={COLORS.white} />
          </Pressable>
        </View>
      </View>
    </TouchableOpacity>
  );
  return (
    <View style={{ flex: 1, backgroundColor: COLORS.white }}>
      <Spinner
        visible={visible}
        textContent="Loading..."
        textStyle={{
          fontSize: 14,
          color: COLORS.white,
        }} />
      <ImageBackground style={styles.image} source={require("../../assets/images/house3.jpg")}>
        <View style={{ flex: 1, alignItems: "center" }}>
          <Image source={require("../../assets/images/person.jpg")}
                 style={{
                   position: "absolute",
                   bottom: -20,
                   borderColor: COLORS.white,
                   borderWidth: 2,
                   height: 80,
                   width: 80,
                   borderRadius: 50,
                 }} />
        </View>
        <View style={{
          position: "absolute",
          right: 20,
          top: 30,
        }}>
          <TouchableOpacity
            style={{
              backgroundColor: COLORS.white,
              height: 30,
              width: 30,
              alignItems: "center",
              justifyContent: "center",
              borderRadius: 16,
            }}
            onPress={() => {
              props.navigation.navigate("SettingAccount");
            }}
          >
            <IconAnt name="setting" color={COLORS.dark} size={18} />
          </TouchableOpacity>
        </View>
      </ImageBackground>
      <View style={{ alignItems: "center", marginTop: 20 }}>
        <Text style={{ fontWeight: "bold", color: COLORS.dark, fontSize: 18 }}>Nguyen Thu Thuy</Text>
      </View>
      <View style={{ marginTop: 8 }}>
        <Text
          style={{ fontWeight: "bold", fontSize: 18, color: COLORS.dark, paddingHorizontal: 20 }}>Note
          List</Text>
        <View style={{ paddingHorizontal: 20, marginTop: 4 }}>
          <View style={styles.picker}>
            <Picker
              selectedValue={selectedNoteType}
              onValueChange={(itemValue) => setSelectedNoteType(itemValue)}
              style={{ placeholderTextColor: "#fff" }}
            >
              <Picker.Item label="All" value="all" style={{ fontSize: 14 }} />
              <Picker.Item label="Type" value="House Type" style={{ fontSize: 14 }} />
              <Picker.Item label="Price" value="House Price" style={{ fontSize: 14 }} />
              <Picker.Item label="Address" value="House Address" style={{ fontSize: 14 }} />
              <Picker.Item label="Bedrooms" value="House Bedrooms" style={{ fontSize: 14 }} />
              <Picker.Item label="Furnished Type" value="House Furniture Types" style={{ fontSize: 14 }} />
            </Picker>
          </View>
        </View>
      </View>
      {notes.length === 0 ? (
        <View style={{ flex: 1, alignItems: "center", justifyContent: "center" }}>
          <Text style={{ color: COLORS.grey }}>No Notes</Text>
        </View>
      ) : (
        <View style={{ flex: 1, marginTop: 10 }}>
          <FlatList
            data={notes}
            renderItem={renderNoteCard}
            keyExtractor={(item) => item.noteId}
            contentContainerStyle={{ paddingVertical: 8, paddingHorizontal: 4, marginBottom: 20 }}
            showsVerticalScrollIndicator={true}
          />
        </View>
      )}
    </View>
  );
};


const styles = StyleSheet.create({
  picker: {
    justifyContent: "center",
    height: 45,
    backgroundColor: COLORS.light,
    marginBottom: 5,
    borderRadius: 6,
  },
  image: {
    height: height * 0.2,
    width: "100%",
  },
  card: {
    height: 100,
    flexDirection: "row",
  },
  main: {
    flex: 1,
    backgroundColor: COLORS.white,
    marginTop: -(height * 0.2),
    borderRadius: 30,
    paddingHorizontal: 40,
    marginBottom: (height * 0.08),
  },
});
export default Profile;
