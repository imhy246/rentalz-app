import React, { useState } from "react";
import { Alert, FlatList, Image, StyleSheet, Text, TouchableOpacity, View } from "react-native";
import COLORS from "../../consts/colors";
import Icon from "react-native-vector-icons/FontAwesome5";
import category from "../../consts/categoryHome";
import houses from "../../consts/house";
import IconAnt from "react-native-vector-icons/AntDesign";

const FavouriteList = props => {
  const [selectedCategory, setSelectedCategory] = useState(0);

  const renderHomeCard = ({ item }) => {
    console.log("house", item);
    return (
      <TouchableOpacity
        style={{
          marginBottom: 16,
          borderRadius: 16,
          backgroundColor: COLORS.white,
          marginHorizontal: 20,
          elevation: 10,
        }}
        onPress={() => {
          props.navigation.navigate("DetailScreen", item);
        }}
      >
        <View style={style.card}>
          <Image source={item.image} style={style.cardImage} resizeMode="cover" />
          <View style={{ margin: 10, flex: 1 }}>
            <Text style={{ fontSize: 17, fontWeight: "bold", color: COLORS.dark, marginBottom: 4 }}>{item.title}</Text>
            <View style={{ flexDirection: "row", marginBottom: 4 }}>
              <Text style={{ fontWeight: "bold", marginRight: 8, color: COLORS.primary }}>{item.price}</Text>
              <View style={{ flexDirection: "row", alignItems: "center" }}>
                <Icon name="bed" size={14} color={COLORS.primary} style={{ marginRight: 3 }} />
                <Text style={{ color: COLORS.primary }}>2</Text>
              </View>
            </View>
            <View style={{ flexShrink: 1 }}>
              <Text numberOfLines={1}>{item.location}</Text>
            </View>
          </View>
          <View style={style.btn}>
            <IconAnt name="heart" color={COLORS.red} style={{
              marginTop: 15,
              height: 40,
              width: 40,
              backgroundColor: COLORS.white,
              borderRadius: 8,
              alignItems: "center",
              justifyContent: "center",
            }} size={20}
                     onPress={() => {
                       Alert.alert(
                         "Are you sure to unfavored this property?",
                         "This action can not undo, so do you want to continue?",
                         [
                           {
                             text: "Cancel", onPress: () => console.log("cancel"), style: "cancel",
                           }, {
                           text: "OK", onPress: () => {
                             //  unlike
                           },
                         }]);
                     }} />
          </View>

        </View>
      </TouchableOpacity>
    );
  };

  return (
    <View style={{ flex: 1, backgroundColor: COLORS.white }}>
      <View style={{ flex: 1, marginTop: 20 }}>
        <Text style={{ fontWeight: "bold", fontSize: 18, color: COLORS.dark, paddingHorizontal: 20 }}>Favorite
          List</Text>
        <FlatList
          data={houses}
          renderItem={renderHomeCard}
          keyExtractor={(item) => item.id}
          contentContainerStyle={{ paddingVertical: 8, paddingHorizontal: 4, marginBottom: 20 }}
          showsVerticalScrollIndicator={true}
        />
      </View>
    </View>
  );
};

const style = StyleSheet.create({
  card: {
    height: 100,
    flexDirection: "row",
  },
  cardImage: {
    height: 100,
    width: "30%",
    borderTopLeftRadius: 16,
    borderBottomLeftRadius: 16,
  },

});
export default FavouriteList;
