import React, { useEffect, useState } from "react";
import { View, Text, Image, TouchableOpacity, Alert } from "react-native";
import { Menu, MenuItem } from "react-native-material-menu";
import Database from "../../../services/db.service";

const CustomMaterialMenu = (props) => {
  let _menu = null;
  const [visible, setVisible] = useState(false);
  const house = props.route.params;

  return (
    <View style={props.menuStyle}>
      <Menu
        visible={visible}
        onRequestClose={() => setVisible(false)}
        ref={(ref) => (_menu = ref)}
        anchor={
          props.isIcon ? (
            <TouchableOpacity onPress={() => _menu.show()}>
              <Image source={{
                uri:
                  "https://raw.githubusercontent.com/AboutReact/sampleresource/master/menu_icon.png",
              }}
                     style={{ width: 30, height: 30 }}
              />
            </TouchableOpacity>
          ) : (
            <Text
              onPress={() => _menu.show()}
              style={props.textStyle}>
              {props.menuText}
            </Text>
          )
        }>
        <MenuItem
          onPress={() => {
            props.navigation.navigate("AddProduct", {
              houses: props.route,
            });
            _menu.hide();
          }}>
          Edit
        </MenuItem>
        <MenuItem
          onPress={() => {
            Alert.alert(
              "Do you want to delete this house?",
              "This action can not undo, so do you want to delete?",
              [],
            );
            _menu.hide();
          }}>
          Delete
        </MenuItem>
      </Menu>

    </View>
  );
};

export default CustomMaterialMenu;
