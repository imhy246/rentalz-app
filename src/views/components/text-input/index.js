import React from "react";
import { Text, TextInput, View, StyleSheet } from "react-native";
import COLORS from "../../../consts/colors";
const CustomTextInput= props => {
  return(
    <View>
      <Text style={style.title}>{props.title}</Text>
      <TextInput
        placeholder={props.placeholder}
        keyboardType="default"
        style={style.input}
      />
    </View>
  )
}

const style = StyleSheet.create({
  title: {
    marginBottom: 5,
    color: COLORS.grey1
  },
  input: {
    borderRadius: 4,
    height: 40,
    borderWidth: 0.3,
    padding: 10,
  }
})
export default CustomTextInput
