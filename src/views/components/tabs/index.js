import React from "react";
import { createBottomTabNavigator } from "@react-navigation/bottom-tabs";
import HomeScreen from "../../screens/HomeScreen";
import FavouriteList from "../../screens/FavouriteList";
import Icon from "react-native-vector-icons/FontAwesome";
import COLORS from "../../../consts/colors";
import Search from "../../screens/Search";
import Profile from "../../screens/Profile";
import AddProduct from "../../screens/AddProduct";

const Tabs = props => {
  const Tabs = createBottomTabNavigator();
  return (
    <Tabs.Navigator
      screenOptions={({ route }) => ({
        headerShown: false,
        tabBarShowLabel: false,
        tabBarIcon: ({ focused }) => {
          const tintColor = focused ? COLORS.primary : COLORS.grey;
          switch (route.name) {
            case "Home":
              return (
                <Icon name="home" size={24} color={tintColor} />
              );
            case "Favorite List":
              return (
                <Icon name="heart" size={24} color={tintColor} />
              );
            case "Search":
              return (
                <Icon name="search" size={24} color={tintColor} />
              );
            case "Profile":
              return (
                <Icon name="user-circle" size={24} color={tintColor} />
              );
            case "Add":
              return (
                <Icon name="plus-square-o" size={24} color={tintColor} />
              );
          }
        },
      })}
    >
      <Tabs.Screen name="Home" component={HomeScreen} />
      <Tabs.Screen name="Search" component={Search} />
      <Tabs.Screen name="Add" component={AddProduct}/>
      <Tabs.Screen name="Favorite List" component={FavouriteList} />
      <Tabs.Screen name="Profile" component={Profile} />
    </Tabs.Navigator>
  );
};
export default Tabs;
