import React from "react";
import { Text, StyleSheet } from "react-native";
import COLORS from "../../../consts/colors";

const Error = props => {
  return (
    <Text style={styles.err}>
      {props.children}
    </Text>
  );
};
const styles = StyleSheet.create({
  err: {
    color: COLORS.red,
    marginBottom: 5,
  },
});
export default Error;
