import React from "react";
import { NavigationContainer } from "@react-navigation/native";
import { createNativeStackNavigator } from "@react-navigation/native-stack";
import Home from "./src/views/screens/HomeScreen";
import OnBoardScreen from "./src/views/screens/OnBoardScreen";
import DetailsScreen from "./src/views/screens/DetailsScreen";
import Tabs from "./src/views/components/tabs";
import ListHouse from "./src/views/screens/ListHouse";
import COLORS from "./src/consts/colors";
import CustomMaterialMenu from "./src/views/components/custom-menu";
import AddProduct from "./src/views/screens/AddProduct";
import AddNote from "./src/views/screens/AddNote";
import SettingAccount from "./src/views/screens/SettingAccount";
import Login from "./src/views/screens/Login";

const Stack = createNativeStackNavigator();
const App = () => {
  return (
    <NavigationContainer>
      <Stack.Navigator
        screenOptions={{
          // headerShown: false,
        }}
        initialRouteName="OnBoardScreen"
      >
        <Stack.Screen name="OnBoardScreen" component={OnBoardScreen} options={{ headerShown: false }} />
        <Stack.Screen name="Overview" component={Tabs} options={{ headerShown: false }} />
        <Stack.Screen name="AddProduct" component={AddProduct} options={{ headerShown: false }} />
        <Stack.Screen name="Home" component={Home} options={{ headerShown: false }} />
        <Stack.Screen name="Login" component={Login} options={{ headerShown: false }} />
        <Stack.Screen name="AddNote" component={AddNote} options={{
          headerTitle: "Add Note",
          headerStyle: {
            backgroundColor: COLORS.blue,
          },
          headerTintColor: "#fff",
        }} />
        <Stack.Screen name="SettingAccount" component={SettingAccount} options={{
          headerTitle: "Setting Account",
          headerStyle: {
            backgroundColor: COLORS.blue,
          },
          headerTintColor: "#fff",
        }} />
        <Stack.Screen name="DetailScreen" component={DetailsScreen} options={({ route, navigation }) => ({
          title: "Detail information",
          headerStyle: {
            backgroundColor: COLORS.blue,
          },
          headerTintColor: "#fff",
          headerTitleStyle: {},
          headerRight: () => (
            <CustomMaterialMenu
              menuText="Menu"
              menuStyle={{ marginRight: 8 }}
              textStyle={{ color: COLORS.white }}
              navigation={navigation}
              route={route}
              isIcon={true}
            />
          ),
        })} />
        <Stack.Screen name="HouseList" component={ListHouse} options={{
          title: "Property List",
          headerStyle: {
            backgroundColor: COLORS.blue,
          },
          headerTintColor: "#fff",
          headerTitleStyle: {
            fontWeight: "bold",
          },
        }} />
      </Stack.Navigator>
    </NavigationContainer>
  );
};
export default App;
